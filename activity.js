//add users 

db.users.insertMany([
    {
        "firstName": "Mike",
        "lastName": "Armstrong",
        "email": "Armstrong@mail.com",
        "password":"metalgarden",
        "isAdmin": false
    },

    {
       "firstName": "Aldrin",
        "lastName": "Hammet",
        "email": "ahammet@mail.com",
        "password":"metalgarden01",
        "isAdmin": false
     },

 {
       "firstName": "Moss",
        "lastName": "Talisman",
        "email": "mtalisman@mail.com",
        "password":"metalgarden02",
        "isAdmin": false
     },

 {
       "firstName": "Kirk",
        "lastName": "Judas",
        "email": "kjudas@mail.com",
        "password":"metalgarden03",
        "isAdmin": false
     },
  {
       "firstName": "Pedro",
        "lastName": "Penduko",
        "email": "ppenduko@mail.com",
        "password":"metalgarden04",
        "isAdmin": false
     }    

])


//add 3 courses

db.courses.insertMany([
    {
        "name": "Javascript",
        "price": 3500,
        "isActive": false
    },

    {
       "name": "React",
        "price": 4500,
        "isActive": false
     },

 {
 "name": "CSS",
        "price": 3000,
        "isActive": false
 }

])


//Read -find all regular/non admin usera
db.users.find({isAdmin: false})


//update 
// update the first users as an admin


db.users.updateOne(

    {isAdmin: false},

    {$set:{ isAdmin: true}}  

)




// update one of the courses as active

db.courses.updateOne(

    {isActive: false},

    {$set:{ isActive: true}}  


)


//delete all
db.courses.deleteMany({isActive:false})



